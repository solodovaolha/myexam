class Book {
    constructor(id, title, author, date, publisher_name, pages, count) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.date = date;
        this.publisher_name = publisher_name;
        this.pages = pages;
        this.count = count;
    }

}

$('.openmodal').click(function (e) {
    e.preventDefault();
    $('.modalwrapper').addClass('modal-show');
});
$('.closemodal').click(function (e) {
    e.preventDefault();
    $('.modalwrapper').removeClass('modal-show');

});

$('.create-visitor').click(function (e) {
    var name = $("#inpt-fname").val();
    var phone = $("#inpt-phone").val();
    var id = localStorage.length + 1;
    const visitor = new Visitor(id, name, phone);
    alert(visitor.show())
    localStorage.setItem(String(id), JSON.stringify(visitor));

    e.preventDefault();
    $('.modalwrapper').removeClass('modal-show');
    $("#inpt-fname").val('');
    $("#inpt-phone").val('');


});




for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    var tempdata = JSON.parse(localStorage.getItem(key));
    $(".divTableBody").after(
        "<div class=\"divTableRow\">\
                    <div class=\"divTableCell keyid-blockjs\">"+ key + "</div>\
                    <div class=\"divTableCell\">"+ tempdata.fullname + "</div>\
                    <div class=\"divTableCell\">"+ tempdata.phone + "</div>\
                    <div class=\"divTableCell edit-blockjs\"><i class=\"fas fa-pencil-alt\"></i></div>\
                </div>"
    )
}


$(".edit-blockjs").click(function (e) {
    
    e.preventDefault();
    $('.modalwrapper').addClass('modal-show');
});

